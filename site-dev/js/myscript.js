$(document).ready(function () {

	$('body').addClass('js')

	var $menu = $('#menu'),
		$menulink = $('.menu-link'),
		$links = $('#menu').find('a')

	$menulink.click(function () {
		$menulink.toggleClass('active')
		$menu.toggleClass('active')
		return false
	});

	$links.click(function () {
		$menulink.toggleClass('active')
		$menu.toggleClass('active')
	});

	$('.rslides').responsiveSlides()

});

var mq48 = window.matchMedia('(min-width:48em)')

function mediaQueries(mq48) {
	// review: Video Batman https://www.youtube.com/watch?v=w7CjtirH_-Y

	if (mq48.matches) {
		//mostrar iframe de video de youtube
		document.getElementById('section-video').innerHTML = '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/w7CjtirH_-Y?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
	} else {
		//mostrar link a video de youtube
		document.getElementById('section-video').innerHTML = '<a class="button-video" href="https://www.youtube.com/watch?v=w7CjtirH_-Y" target="_blank">Ver video</a>'
	}
}

mediaQueries(mq48)
mq48.addListener(mediaQueries)